import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Folios } from '../models/folios';
@Injectable({
  providedIn: 'root'
})
export class FoliosService {

  selectedFolio: Folios;
  Folios: Folios[];

  readonly URL_API = 'http://localhost:3000/';

  constructor(private http: HttpClient) {
    this.selectedFolio = new Folios();
  }

  postFolio(folio: Folios) {
    return this.http.post(this.URL_API, folio);
  }

  getFolios() {
    return this.http.get(this.URL_API);
  }

  putFolio(folios: Folios) {
    return this.http.put(this.URL_API + `/${folios.number}`, folios);
  }

  deleteFolio(number: Number) {
    return this.http.delete(this.URL_API + `/${number}`);
  }
}
