export class Folios {

    constructor(number = 0, status = '') {
        this.number =  number;
        this.status = status;
    }

    number: Number;
    status: string;
   
}