export interface UserI {
    id:            number,
    f_name:        String,  
    l_name:        String,  
    date_b:        String,  
    departamento:  String,  
    email:         String,  
    password:      String
}
