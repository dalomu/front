import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { NoPagefoundComponent } from './pages/no-pagefound/no-pagefound.component';
import { PagesComponent } from './pages/pages.component'
const routes: Routes = [
    {
      path: '', 
      component: PagesComponent,
      children:[
        {path: 'dashboard', component: DashboardComponent },
        {path: '', redirectTo: 'dashboard', pathMatch: 'full'}
      ]
    },

    // {path:'auth', loadChildren:'./auth/auth.module#AuthModule'},
    // { path: 'auth', loadChildren: () => import('src/app/auth/auth.module').then(m => m.AuthModule) },

    {path: 'login', component: LoginComponent  },
    {path: 'register', component: RegisterComponent},
    
    {path: '**', component: NoPagefoundComponent },

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports:[ RouterModule ]
})
export class AppRoutingModule { }
