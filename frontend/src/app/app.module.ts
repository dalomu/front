import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//PARA LAS FORMS Y ACCEDER A LA API
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


//MODULOS 
import { AuthModule } from './auth/auth.module';

//RUTAS
import { AppRoutingModule } from './app-routing.module';
//SERVICIOS
import { AuthService} from './services/auth.service'


//COMPONENTES QUE SE MOVERÁN EN UN FUTURO
import { AppComponent } from './app.component';
import { FoliosComponent } from './components/folios/folios.component';
// import { LoginComponent } from './auth/login/login.component';
// import { RegisterComponent } from './auth/register/register.component';
import { NoPagefoundComponent } from './pages/no-pagefound/no-pagefound.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { BreadcrumbsComponent } from './shared/breadcrumbs/breadcrumbs.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { HeaderComponent } from './shared/header/header.component';
import { PagesComponent } from './pages/pages.component';


@NgModule({
  declarations: [
    AppComponent,
    FoliosComponent,
    // LoginComponent,
    // RegisterComponent,
    NoPagefoundComponent,
    DashboardComponent,
    BreadcrumbsComponent,
    SidebarComponent,
    HeaderComponent,
    PagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AuthModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
