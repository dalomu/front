import { Component, OnInit } from '@angular/core';


import { FoliosService } from '../../services/folios.service';
import { NgForm } from '@angular/forms';
import { Folios } from '../../models/folios';

declare var M: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ FoliosService ]
})
export class DashboardComponent implements OnInit {

  constructor(public folioService: FoliosService) { }

  ngOnInit(): void {
    this.getFolios();
  }
  
  addFolio(form?: NgForm) {
    console.log(form.value);
    if(form.value.number) {
      this.folioService.putFolio(form.value)
        .subscribe(res => {
          this.resetForm(form);
          this.getFolios();
          M.toast({html: 'Updated Successfully'});
        });
    } else {
      this.folioService.postFolio(form.value)
      .subscribe(res => {
        this.getFolios();
        this.resetForm(form);
        M.toast({html: 'Save successfully'});
      });
    }
    
  }

  getFolios() {
    this.folioService.getFolios()
      .subscribe(res => {
        this.folioService.Folios = res as Folios[];
      });
  }

  editFolio(folio: Folios) {
    this.folioService.selectedFolio = folio;
  }

  deleteFolio(number: Number, form: NgForm) {
    if(confirm('Are you sure you want to delete it?')) {
      this.folioService.deleteFolio(number)
        .subscribe(res => {
          this.getFolios();
          this.resetForm(form);
          M.toast({html: 'Deleted Succesfully'});
        });
    }
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.folioService.selectedFolio = new Folios();
    }
  }
}
