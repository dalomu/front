import { NgModule } from '@angular/core';
import { CommonModule, FormStyle } from '@angular/common';


import {FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//servicios
import { AuthService} from '../services/auth.service'
//components
import { AppRoutingModule } from '../app-routing.module'
import { RegisterComponent  } from './register/register.component';
import { LoginComponent } from "./login/login.component";

@NgModule({
  declarations: [ RegisterComponent, LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers:[AuthService]
})
export class AuthModule { }
