import { Component, OnInit } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { UserI } from '../../models/user';
import { AuthModule } from '../auth.module';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(
    //SALE ERROR CUANDO ACCEDES AL /login probablemente sea relacionado con el servicio
    private authService: AuthService, private router: Router
    ) { }

  ngOnInit(): void {
  }


  onLogin(form):void {
     console.log('Login', form.value);
  }
}
