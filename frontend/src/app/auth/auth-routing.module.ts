

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [

    // {path: '/login', component: LoginComponent  },
    // {path: '/register', component: RegisterComponent},
    // {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    // {path: '**', component: NoPagefoundComponent },

];                         

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports:[ RouterModule ]
})
export class AuthRoutingModule { }
